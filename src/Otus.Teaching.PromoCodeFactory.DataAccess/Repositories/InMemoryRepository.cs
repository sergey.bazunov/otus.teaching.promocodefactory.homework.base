﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> Append(T NewEntity)
        {
            if (Data.Any(e => e.Id == NewEntity.Id))
            {
                return Task.FromResult<T>(null);
            }

            Data.Add(NewEntity);
            return Task.FromResult(NewEntity);
        }

        public Task<T> Update(T Element)
        {
            T e = Data.FirstOrDefault(e => e.Id == Element.Id);

            if (e == null)
            {
                return Task.FromResult<T>(null);
            }

            Data.Remove(e);
            Data.Add(Element);
            return Task.FromResult(Element);
        }

        public Task<T> Delete(Guid id)
        {
            T e = Data.FirstOrDefault(e => e.Id == id);

            if (e == null)
            {
                return Task.FromResult<T>(null);
            }

            Data.Remove(e);
            return Task.FromResult(e);
        }
    }
}