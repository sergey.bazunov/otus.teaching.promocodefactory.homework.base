﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepo;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> rolesRepo)
        {
            _employeeRepository = employeeRepository;
            _rolesRepo = rolesRepo;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),

                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создать нового сотрудника
        /// </summary>
        /// <returns>Данные нового сотрудника</returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeShortResponse>> CreateNewEmployee(EmployeeDto employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var e = new Employee()
            {
                Id = Guid.NewGuid(),
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Email = employee.Email,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            e.Roles = new List<Role>(await FindRoles(employee.RequestedRoles));

            var newbie = await _employeeRepository.Append(e);

            return Created(new Uri(Request.Path + "/" + newbie.Id.ToString(), UriKind.Relative),
                new EmployeeShortResponse()
                {
                    Id = newbie.Id,
                    FullName = newbie.FullName,
                    Email = newbie.Email
                }
            );
        }

        /// <summary>
        /// Обновить данные сотрудника по Id
        /// </summary>
        /// <returns>Возвращаем сотрудника с обновленными данными</returns>
        [HttpPut]
        public async Task<ActionResult<EmployeeShortResponse>> UpdateEmployee(Guid id, EmployeeDto employee)
        {
            var found = await _employeeRepository.GetByIdAsync(id);
            if (found == null)
                return NotFound();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            found.FirstName = employee.FirstName;
            found.LastName = employee.LastName;
            found.Email = employee.Email;
            found.AppliedPromocodesCount = employee.AppliedPromocodesCount;

            found.Roles.Clear();
            found.Roles.AddRange(await FindRoles(employee.RequestedRoles));

            var e = await _employeeRepository.Update(found);

            return Ok(new EmployeeShortResponse()
            {
                Id = e.Id,
                FullName = e.FullName,
                Email = e.Email
            });
        }

        /// <summary>
        /// Удалить сотрудника по Id
        /// </summary>
        /// <returns>Возвращаем удалнного сотрудника</returns>
        [HttpDelete]
        public async Task<ActionResult<EmployeeShortResponse>> DeleteEmployee(Guid id)
        {
            var e = await _employeeRepository.Delete(id);

            if (e == null)
                return NotFound();

            return Ok(new EmployeeShortResponse()
            {
                Id = e.Id,
                FullName = e.FullName,
                Email = e.Email
            });
        }

        /// <summary>
        ///  Create Roles from requested valid guids
        /// </summary>
        private async Task<IEnumerable<Role>> FindRoles(List<Guid> guids)
        {
            var NewRoles = new List<Role>();
            foreach (Guid roleId in guids)
            {
                var found = await _rolesRepo.GetByIdAsync(roleId);
                if (found != null)
                {
                    NewRoles.Add(found);
                }
            }
            return NewRoles;
        }

    }
}